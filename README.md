# Gondola Patch

An extension patch for Mega Man 8-Bit Deathmatch's Class Based Modification addon.

[Join the Discord!](https://discord.gg/HbZppcfGxj)

# Credits

Mega Man 8-Bit Deathmatch: https://cutstuff.net/mm8bdm/

Class Based Modification: https://cutstuff.net/forum/index.php?topic=12004.0

