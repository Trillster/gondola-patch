# Contributing to Gondola Patch

This document will detail information regarding making contributions and pull requests in the Gondola Patch project. To keep things organized and maintainable, please abide by the following guidelines!

# CBINS

This project is compiled using [Can't Believe It's Not SLADE (CBINS)](https://gitlab.com/Trillster/cbins), a PK3 compilation tool. Refer to the hyperlinked repository for a usage guide.

# Pull Requests

When creating new features, bugfixes, or balance changes, you should use a meaningful branch name. Use a prefix of `feat/`, `bug/`, or `balance/` depending on the change. Below are a few examples:
```
feat/ring-rework
bug/sticky-protoman-shield
balance/coldman
```

Keep the branches which implement these changes very small and concise in scope. If you're working on two features, make it two different branches!

Features, bugfixes, and balance changes should all be implemented through pull requests into the `dev` branch. When creating a new change, you should create a new branch off of `dev` by using the following Git workflow.
```bash
git checkout dev
git pull origin dev
git checkout -b feat/new-feature
```
Once you've finished your changes, you should commit them, merge dev into your feature branch one more time in case it has gotten stale, then push the feature branch to origin.
```bash
git add .
git commit -m "A good commit message!"
git pull origin dev
git push origin feat/new-feature
```
Once you've done this, you can open a pull request to bring your feature branch into `dev` on GitLab.

If you don't have access as contributor to the repository but still wish to submit changes, you can create a fork and submit a pull request that way.

The `dev` branch will be merged into `main` whenever a new version of Gondola Patch is ready to be released.

# Changelog

Whenever making changes, do go ahead and additionally update the `CHANGELOG.txt` file in the `gondolapatch-pk3` directory as well as the internal changelog in the PK3's `MENUDEF` files.

Keep the change descriptions in the `MENUDEF` files brief and non-technical, these are for the average player, not other developers!

# Think V6B

This version of Gondola Patch runs on Class Based Modification V9GH + MM8BDM V6B onwards, so before anything else, please be familiar with creating content for these versions. If you're unfamiliar with these practices, [read the pages on the MM8BDM wiki](https://mm8bdm.notion.site/Interacting-with-Systems-c99b6cabaa4f4964928b2af9c7e0a7ba).

Abiding by the standards of MM8BDM V6B onwards allows Gondola Patch to be compatible with many different types of content expansions.

Whenever introducing new game wide features, you should always consider how it can be designed to allow additional addons to make use of it (see: automap class icons and overhead ammo bars). 

These two guidelines combined will allow for people to create extra class addons without needing to tinker with additional Gondola Patch compatibility files.

# Leverage Existing Code

Between MM8BDM V6B+ and CBM, a lot of common functionality is already implemented and can leveraged through existing scripts and actors. Only implement a new script or basic actor if it's absolutely needed for some extra functionality not accomplishable with existing tools. 

If you're bringing in some utility code, you should really think hard about why you're bringing it in, because chances are, it's not necessary. Don't reinvent the wheel!

# Document File Replacements

Replacing files is generally a bad idea for long term maintainability, especially when untracked. Whenever possible, you should prefer to create new ACS and DECORATE files with the content which you are planning to implement.

If you must replace a file from MM8BDM or CBM to override functionality, you should keep the added / removed code as minimal as possible, putting as much as possible in new files. 

For example, rather than replacing a file to add a new actor and use that actor in an existing inventory actor, you should implement that new actor in its own file, that way the replacement only needs to update the inventory actor.

In those cases which you have to do a replacement, you should document the file you're replacing in `REPLACED.md`. Please note the full file path, which file it came from, and the reason for the replacement.
```md
actors/CBM/CBM_Modder.txt
- classes-v9gh.pk3 
- Removed Beef_CBMModderSupport_H
```
In the file which exists to replace an existing DECORATE / ACS file, you should also leave a comment at the top explaining the replacement being done and comments denoted with `[GOND]` near the changes. These comments are **not** required for any replacements of class weapon or actor files, but you should still note the replacements in `REPLACED.md`.

Whenever looking to create a pull request which is implementing a replacing file, create one commit that has the unmodified file, then another commit which implements the changes. This will make it easier to review these changes for any errors. Below is an example of this workflow:
```bash
git checkout dev
git pull origin dev
git checkout -b feat/sr50-indicator
# implement unmodified file(s) to be replaced
git add .
git commit -m "Prep for replacing files"
# now make changes to the file
git add .
git commit -m "Changes to replaced file"
git push origin feat/sr50-indicator
```